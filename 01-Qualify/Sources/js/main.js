/// <reference path="frameworks/jquery-1.7.2.min.js" />
/// <reference path="plugins/jquery.simpleplaceholder.js" />

(function ($, undefined)
{
    $(document).ready(function ()
    {
        // enable input placeholders
        $('textarea[placeholder]').simplePlaceholder();
        $('input[placeholder]').simplePlaceholder(); // classic input[type=text]

        var $mainMenu = $('.main-menu');
        $mainMenu.delegate('a', 'click', function (event)
        {
            $mainMenu.find('li').removeClass('active');
            $(event.target).closest('li').addClass('active');
        });
    });

})(jQuery);
